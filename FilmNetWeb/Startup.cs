﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FilmNetWeb.Startup))]
namespace FilmNetWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
