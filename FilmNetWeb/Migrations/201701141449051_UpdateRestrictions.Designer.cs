// <auto-generated />
namespace FilmNetWeb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class UpdateRestrictions : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UpdateRestrictions));
        
        string IMigrationMetadata.Id
        {
            get { return "201701141449051_UpdateRestrictions"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
