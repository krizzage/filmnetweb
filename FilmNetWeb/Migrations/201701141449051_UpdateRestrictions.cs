namespace FilmNetWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateRestrictions : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Actors", "Surname", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Actors", "Surname", c => c.String());
        }
    }
}
