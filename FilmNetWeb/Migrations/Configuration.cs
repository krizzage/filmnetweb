namespace FilmNetWeb.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using Models.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FilmNetWeb.DAL.FilmNetContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FilmNetWeb.DAL.FilmNetContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            roleManager.Create(new IdentityRole("Administrator"));

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var user = new ApplicationUser { UserName = "admin" };
            userManager.Create(user, "admin1");
            userManager.AddToRole(user.Id, "Administrator");

            context.SaveChanges();

            var genres = new List<Genre>
            {
                new Genre() { Name="Akcja" },
                new Genre() { Name="Dramat" },
                new Genre() { Name="Komedia" },
                new Genre() { Name="Wojenny" }
            };

            genres.ForEach(g => context.Genres.Add(g));
            context.SaveChanges();

            var films = new List<Film>
            {
                new Film() { Name = "Zielona mila", Description = "Emerytowany stra�nik wi�zienny opowiada przyjaci�ce o niezwyk�ym m�czy�nie, kt�rego skazano na �mier� za zab�jstwo dw�ch 9-letnich dziewczynek.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Dramat"), Year=1999, Average=0 },
                new Film() { Name = "Forrest Gump", Description = "Historia �ycia Forresta, ch�opca o niskim ilorazie inteligencji z niedow�adem ko�czyn, kt�ry staje si� miliarderem i bohaterem wojny w Wietnamie.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Dramat"), Year=1994, Average=0 },
                new Film() { Name = "Szeregowiec Ryan", Description = "W poszukiwaniu zaginionego szeregowca wys�any zostaje doborowy oddzia� �o�nierzy.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Wojenny"), Year=1998, Average=0 },
                new Film() { Name = "Terminal", Description = "Gdy Viktor Navorski, turysta z Europy Wschodniej, przylatuje do Nowego Jorku, w jego ojczy�nie ma miejsce zamach stanu. Ameryka�skie w�adze nie uznaj� paszportu m�czyzny, wi�c miejscem przymusowego pobytu staje si� dla niego hala terminalu.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Komedia"), Year=2004, Average=0 },
                new Film() { Name = "Maska Zorro", Description = "Gubernator w Meksyku, Montero, morduje �on� Zorro, porywa c�rk�, a jego samego wtr�ca do wi�zienia. Po latach Don Diego szuka nast�pcy, kt�ry zem�ci si� na wrogu.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Akcja"), Year=1998, Average=0 },
                new Film() { Name = "Brazowa mila", Description = "Emerytowany stra�nik wi�zienny opowiada przyjaci�ce o niezwyk�ym m�czy�nie, kt�rego skazano na �mier� za zab�jstwo dw�ch 9-letnich dziewczynek.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Dramat"), Year=1990, Average=0 },
                new Film() { Name = "Noob Gump", Description = "Historia �ycia Forresta, ch�opca o niskim ilorazie inteligencji z niedow�adem ko�czyn, kt�ry staje si� miliarderem i bohaterem wojny w Wietnamie.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Dramat"), Year=2221, Average=0 },
                new Film() { Name = "Weteran Ryan", Description = "W poszukiwaniu zaginionego szeregowca wys�any zostaje doborowy oddzia� �o�nierzy.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Wojenny"), Year=1998, Average=0 },
                new Film() { Name = "Test132", Description = "Gdy Viktor Navorski, turysta z Europy Wschodniej, przylatuje do Nowego Jorku, w jego ojczy�nie ma miejsce zamach stanu. Ameryka�skie w�adze nie uznaj� paszportu m�czyzny, wi�c miejscem przymusowego pobytu staje si� dla niego hala terminalu.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Komedia"), Year=1994, Average=0 },
                new Film() { Name = "Zorro maski", Description = "Gubernator w Meksyku, Montero, morduje �on� Zorro, porywa c�rk�, a jego samego wtr�ca do wi�zienia. Po latach Don Diego szuka nast�pcy, kt�ry zem�ci si� na wrogu.",
                    Genre = context.Genres.SingleOrDefault(x => x.Name == "Akcja"), Year=1998, Average=0 }
            };

            films.ForEach(f => context.Films.Add(f));
            context.SaveChanges();

            var actors = new List<Actor>
            {
                new Actor() { Name="Tom", Surname ="Hanks",
                    Description ="Jego rodzice rozwiedli si�, gdy mia� pi�� lat. Wychowywa� go ojciec � kucharz, kt�ry wkr�tce przeni�s� si� z Kaliforni do Oakland. Aktorstwem Tom zainteresowa� si� w szkole �redniej. " },
                new Actor() { Name="Michael", Surname ="Duncan",
                    Description ="Mierz�cy prawie dwa metry wzrostu Michael Clarke Duncan w Hollywood cz�sto bywa nazywany \"Wielkim Mikiem\" (\"Big Mike\"). M�wi si� o nim jako o olbrzymie z wielkim sercem i ujmuj�c� osobowo�ci�. Publiczno�� kojarzy go przede wszystkim z roli Johna Coffeya z adaptacji powie�ci Stephena Kinga - \"Zielona mila\" w re�yserii Franka Darabonta."},
                new Actor() { Name="Robin", Surname = "Wright",
                    Description ="Robin Wright Penn urodzi�a si� w Dallas (Teksas), dorasta�a jednak w San Diego (Kalifornia) w USA. W wieku 14 lat rozpocz�a profesjonaln� karier� modelki w Pary�u i Japonii. Po sko�czeniu szko�y �redniej zdecydowa�a si� zosta� aktork�." },
                new Actor() { Name="Matt", Surname ="Damon",
                    Description ="Gdy mia� nieca�e dwa lata, jego ojciec opu�ci� rodzin�. W wieku 16 lat wyst�pi� w reklam�wce, ale na prawdziw� popularno�� musia� jednak jeszcze zaczeka�." },
                new Actor() { Name="Catherine", Surname ="Zeta-Jones",
                    Description ="Catherine przysz�a na �wiat 25 wrze�nia 1969 roku w ma�ej, walijskiej miejscowo�ci Swansea - rodzinnym mie�cie poety Dylana Thomasa. Chocia� urodzi�a si� w robotniczej rodzinie bez tradycji aktorskich, aktorstwo by�o jej pisane. Jej ojciec, David, rodowity Walijczyk z wykszta�cenia jest cukiernikiem, natomiast Pat, matka Catherine pochodz�ca z Irlandii jest krawcow�." },
                new Actor() { Name="Antonio", Surname ="Banderas",
                    Description ="Aktor pochodzenia hiszpa�skiego, kt�remu uroda po�udniowca zapewni�a status hollywoodzkiego symbolu seksu lat 90. Mi�dzynarodowy rozg�os zdoby� dzi�ki wsp�pracy ze swoim rodakiem, s�ynnym tw�rc� filmowym, Pedro Almod�varem. Karier� zawodow� zaczyna� w rodzinnym kraju, gdzie po uko�czeniu szko�y teatralnej grywa� w sztukach Christophera Marlowa, Bertolta Brechta i Williama Szekspira." },
                new Actor() { Name="Anthony", Surname ="Hopkins",
                    Description ="Sir Philip Anthony Hopkins urodzi� si� 31 grudnia 1937 roku w walijskim miasteczku Port Talbot. Jest jedynym dzieckiem Dicka i Muriel Hopkins�w." },
                 new Actor() { Name="Tom", Surname ="Hanks",
                    Description ="Jego rodzice rozwiedli si�, gdy mia� pi�� lat. Wychowywa� go ojciec � kucharz, kt�ry wkr�tce przeni�s� si� z Kaliforni do Oakland. Aktorstwem Tom zainteresowa� si� w szkole �redniej. " },
                new Actor() { Name="Michael", Surname ="Jackson",
                    Description ="Mierz�cy prawie dwa metry wzrostu Michael Clarke Duncan w Hollywood cz�sto bywa nazywany \"Wielkim Mikiem\" (\"Big Mike\"). M�wi si� o nim jako o olbrzymie z wielkim sercem i ujmuj�c� osobowo�ci�. Publiczno�� kojarzy go przede wszystkim z roli Johna Coffeya z adaptacji powie�ci Stephena Kinga - \"Zielona mila\" w re�yserii Franka Darabonta."},
                new Actor() { Name="Robinson", Surname = "Kruzo",
                    Description ="Robin Wright Penn urodzi�a si� w Dallas (Teksas), dorasta�a jednak w San Diego (Kalifornia) w USA. W wieku 14 lat rozpocz�a profesjonaln� karier� modelki w Pary�u i Japonii. Po sko�czeniu szko�y �redniej zdecydowa�a si� zosta� aktork�." },
                new Actor() { Name="Mateusz", Surname ="Diamond",
                    Description ="Gdy mia� nieca�e dwa lata, jego ojciec opu�ci� rodzin�. W wieku 16 lat wyst�pi� w reklam�wce, ale na prawdziw� popularno�� musia� jednak jeszcze zaczeka�." },
                new Actor() { Name="Robert", Surname ="Lewandowski",
                    Description ="Catherine przysz�a na �wiat 25 wrze�nia 1969 roku w ma�ej, walijskiej miejscowo�ci Swansea - rodzinnym mie�cie poety Dylana Thomasa. Chocia� urodzi�a si� w robotniczej rodzinie bez tradycji aktorskich, aktorstwo by�o jej pisane. Jej ojciec, David, rodowity Walijczyk z wykszta�cenia jest cukiernikiem, natomiast Pat, matka Catherine pochodz�ca z Irlandii jest krawcow�." },
                new Actor() { Name="Micheal", Surname ="Jordan",
                    Description ="Aktor pochodzenia hiszpa�skiego, kt�remu uroda po�udniowca zapewni�a status hollywoodzkiego symbolu seksu lat 90. Mi�dzynarodowy rozg�os zdoby� dzi�ki wsp�pracy ze swoim rodakiem, s�ynnym tw�rc� filmowym, Pedro Almod�varem. Karier� zawodow� zaczyna� w rodzinnym kraju, gdzie po uko�czeniu szko�y teatralnej grywa� w sztukach Christophera Marlowa, Bertolta Brechta i Williama Szekspira." },
                new Actor() { Name="Anthony", Surname ="Carmelo",
                    Description ="Sir Philip Anthony Hopkins urodzi� si� 31 grudnia 1937 roku w walijskim miasteczku Port Talbot. Jest jedynym dzieckiem Dicka i Muriel Hopkins�w." },
                 new Actor() { Name="Tomek", Surname ="Tomasz",
                    Description ="Jego rodzice rozwiedli si�, gdy mia� pi�� lat. Wychowywa� go ojciec � kucharz, kt�ry wkr�tce przeni�s� si� z Kaliforni do Oakland. Aktorstwem Tom zainteresowa� si� w szkole �redniej. " },
                new Actor() { Name="Marcin", Surname ="Najman",
                    Description ="Mierz�cy prawie dwa metry wzrostu Michael Clarke Duncan w Hollywood cz�sto bywa nazywany \"Wielkim Mikiem\" (\"Big Mike\"). M�wi si� o nim jako o olbrzymie z wielkim sercem i ujmuj�c� osobowo�ci�. Publiczno�� kojarzy go przede wszystkim z roli Johna Coffeya z adaptacji powie�ci Stephena Kinga - \"Zielona mila\" w re�yserii Franka Darabonta."},
                new Actor() { Name="Mariusz", Surname = "Pudzianowski",
                    Description ="Robin Wright Penn urodzi�a si� w Dallas (Teksas), dorasta�a jednak w San Diego (Kalifornia) w USA. W wieku 14 lat rozpocz�a profesjonaln� karier� modelki w Pary�u i Japonii. Po sko�czeniu szko�y �redniej zdecydowa�a si� zosta� aktork�." },
                new Actor() { Name="Chleb", Surname ="Witaj",
                    Description ="Gdy mia� nieca�e dwa lata, jego ojciec opu�ci� rodzin�. W wieku 16 lat wyst�pi� w reklam�wce, ale na prawdziw� popularno�� musia� jednak jeszcze zaczeka�." },
                new Actor() { Name="Johhny", Surname ="Bravo",
                    Description ="Catherine przysz�a na �wiat 25 wrze�nia 1969 roku w ma�ej, walijskiej miejscowo�ci Swansea - rodzinnym mie�cie poety Dylana Thomasa. Chocia� urodzi�a si� w robotniczej rodzinie bez tradycji aktorskich, aktorstwo by�o jej pisane. Jej ojciec, David, rodowity Walijczyk z wykszta�cenia jest cukiernikiem, natomiast Pat, matka Catherine pochodz�ca z Irlandii jest krawcow�." },
                new Actor() { Name="Placki", Surname ="Szimenl",
                    Description ="Aktor pochodzenia hiszpa�skiego, kt�remu uroda po�udniowca zapewni�a status hollywoodzkiego symbolu seksu lat 90. Mi�dzynarodowy rozg�os zdoby� dzi�ki wsp�pracy ze swoim rodakiem, s�ynnym tw�rc� filmowym, Pedro Almod�varem. Karier� zawodow� zaczyna� w rodzinnym kraju, gdzie po uko�czeniu szko�y teatralnej grywa� w sztukach Christophera Marlowa, Bertolta Brechta i Williama Szekspira." },
                new Actor() { Name="Brian", Surname ="Szymanski",
                    Description ="Sir Philip Anthony Hopkins urodzi� si� 31 grudnia 1937 roku w walijskim miasteczku Port Talbot. Jest jedynym dzieckiem Dicka i Muriel Hopkins�w." }
            };

            actors.ForEach(a => context.Actors.Add(a));
            context.SaveChanges();

            var roles = new List<ActorRole>
            {
                new ActorRole() { FilmId=1, ActorId=1, Name="Paul Edgecombe" },
                new ActorRole() { FilmId=1, ActorId=2, Name="John Coffey" },
                new ActorRole() { FilmId=2, ActorId=1, Name="Forrest Gump" },
                new ActorRole() { FilmId=2, ActorId=3, Name="Jenny Curran" },
                new ActorRole() { FilmId=3, ActorId=1, Name="Kapitan John H. Miller" },
                new ActorRole() { FilmId=3, ActorId=4, Name="Szeregowy James Francis Ryan" },
                new ActorRole() { FilmId=4, ActorId=1, Name="Viktor Navorski" },
                new ActorRole() { FilmId=4, ActorId=5, Name="Amelia Warren" },
                new ActorRole() { FilmId=5, ActorId=6, Name="Alejandro Murrieta" },
                new ActorRole() { FilmId=5, ActorId=7, Name="Don Diego de la Vega" },
                new ActorRole() { FilmId=5, ActorId=5, Name="Elena Montero / Elena Murrieta" }
            };

            roles.ForEach(r => context.ActorRoles.Add(r));
            context.SaveChanges();
        }
    }
}
