﻿using FilmNetWeb.DAL;
using FilmNetWeb.DAL.Abstract;
using FilmNetWeb.DAL.Repositories;
using FilmNetWeb.Models.Entities;
using FilmNetWeb.Models.ViewModels;
using FilmNetWeb.Tools;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmNetWeb.Controllers
{
    [Authorize]
    public class FriendsController : Controller
    {
        private IUserRepository userRepository;

        public FriendsController(IUserRepository _userRepository)
        {
            this.userRepository = _userRepository;
        }

        public ActionResult Index()
        {
            var friends = userRepository.GetFriends(User.Identity.GetUserId());

            return View(friends);
        }

        public ActionResult InviteFriend(string status)
        {
            ViewBag.StatusMessage = status;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InviteFriend(InvitationViewModel invitation)
        {
            string statusMessage = "";
            if (ModelState.IsValid)
            {
                var receiver = userRepository.SingleOrDefault(x => x.UserName == invitation.UserName);
                var sender = userRepository.GetByID(User.Identity.GetUserId());
                var status = invitation.Validate(sender.UserName);
                if (status == InvitationValidationMessage.Success)
                {
                    statusMessage = "Użytkownik zaproszony.";
                    userRepository.AddInvitation(sender.Id, receiver.Id);
                    userRepository.Save();
                }
                else
                {
                    statusMessage  =
                        status == InvitationValidationMessage.CannotInviteYourself ? "Nie możesz zaprosić siebie."
                      : status == InvitationValidationMessage.UserDoNotExists ? "Użytkownik nie istnieje."
                      : status == InvitationValidationMessage.UserHasAlreadyInvitedYou ? "Użytkownik już Cię zaprosił."
                      : status == InvitationValidationMessage.UserIsAlreadyFriend ? "Użytkownik już jest Twoim znajomym."
                      : status == InvitationValidationMessage.UserIsAlreadyInvited ? "Użytkownik już jest zaproszony."
                : "";
                }
            }

            return RedirectToAction("InviteFriend", new { status = statusMessage });
        }

        public ActionResult PendingInvitations()
        {
            return View(GetInvitations());
        }

        [HttpPost]
        public ActionResult AcceptInvitation(string senderId)
        {
            userRepository.AcceptInvitation(new Friend() { SenderId = senderId, ReceiverId = User.Identity.GetUserId() });
            userRepository.Save();

            return PartialView("_InvitationsList", GetInvitations());
        }

        [HttpPost]
        public ActionResult DeclineInvitation(string senderId)
        {
            userRepository.DeclineInvitation(new Friend() { SenderId = senderId, ReceiverId = User.Identity.GetUserId() });
            userRepository.Save();

            return PartialView("_InvitationsList", GetInvitations());
        }

        private IEnumerable<PendingInvitationViewModel> GetInvitations()
        {
            var invitations = userRepository.GetPendingInvitations(User.Identity.GetUserId());
            var invitationsViewModel = new List<PendingInvitationViewModel>();

            foreach (var inv in invitations)
            {
                invitationsViewModel.Add(new PendingInvitationViewModel(inv));
            }

            return invitationsViewModel;
        }

        public ActionResult DisplayDetails(string userId)
        {
            var user = userRepository.Get(x => x.Id == userId, includeProperties: "Ratings").First();

            var userViewModel = new UserDetailsViewModel(user);

            return PartialView("_userDetails", userViewModel);
        }
    }
}