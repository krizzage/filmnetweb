﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FilmNetWeb.DAL;
using FilmNetWeb.Models.Entities;
using FilmNetWeb.DAL.Repositories;
using FilmNetWeb.Tools;
using PagedList;
using Microsoft.AspNet.Identity;
using FilmNetWeb.DAL.Abstract;

namespace FilmNetWeb.Controllers
{
    public class FilmsController : Controller
    {
        private IFilmRepository filmsRepository;
        private IRepository<ActorRole> rolesRepository;
        private IRepository<Actor> actorsRepository;
        private IRepository<Genre> genresRepository;
        private IRatingRepository ratingsRepository;

        public FilmsController(IFilmRepository _filmsRepository, IRepository<ActorRole> _rolesRepository, IRepository<Actor> _actorsRepository,
                               IRepository<Genre> _genresRepository, IRatingRepository _ratingsRepository)
        {
            this.filmsRepository = _filmsRepository;
            this.rolesRepository = _rolesRepository;
            this.actorsRepository = _actorsRepository;
            this.genresRepository = _genresRepository;
            this.ratingsRepository = _ratingsRepository;
        }

        public ActionResult Index(string sortOrder, string searchString, string genreFilter, int? page)
        {
            List<SelectListItem> genresCombo = new List<SelectListItem>();

            var genres = genresRepository.Get();
            genresCombo.Add(new SelectListItem() { Text = "" });
            foreach(var g in genres)
            {
                genresCombo.Add(new SelectListItem() { Text = g.Name, Value=g.Name });
            }
            ViewBag.GenresCombo = genresCombo;

            var filmsList = GetFilms(sortOrder, searchString, genreFilter, page);

            return View("Index", filmsList);
        }

        public ActionResult FilmsList(string sortOrder, string searchString, string genreFilter, int? page)
        {
            var filmsList = GetFilms(sortOrder, searchString, genreFilter, page);

            return PartialView("_FilmsList", filmsList);
        }

        [NonAction]
        private IPagedList<Film> GetFilms(string sortOrder, string searchString, string genreFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentSearchString = searchString;
            ViewBag.CurrentGenre = genreFilter;

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.YearSortParm = sortOrder == "year" ? "year_desc" : "year";
            ViewBag.AverageSortParm = sortOrder == "average" ? "average_desc" : "average";

            var filter = ExpressionBuilder.BuildFilmFilter(searchString, genreFilter);
            var sortFilms = ExpressionBuilder.BuildFilmOrderBy(sortOrder);

            var filmsList = filmsRepository.Get(filter, sortFilms, "Genre");

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return filmsList.ToPagedList(pageNumber, pageSize);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Film film = filmsRepository.SingleOrDefault(x => x.FilmId == id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View(film);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            ViewBag.GenreId = new SelectList(genresRepository.Get(), "GenreId", "Name");
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GenreId,Name,Description,Year")] Film film)
        {
            if (ModelState.IsValid)
            {
                filmsRepository.Insert(film);
                filmsRepository.Save();
                return RedirectToAction("Edit", new { id = film.FilmId });
            }

            ViewBag.GenreId = new SelectList(genresRepository.Get(), "GenreId", "Name", film.GenreId);
            return View(film);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Film film = filmsRepository.SingleOrDefault(x => x.FilmId == id);
            if (film == null)
            {
                return HttpNotFound();
            }

            List<SelectListItem> actorsCombo = new List<SelectListItem>();
            var actors = actorsRepository.Get();
            actorsCombo.Add(new SelectListItem() { Text = "" });
            foreach (var a in actors)
            {
                actorsCombo.Add(new SelectListItem() { Text = a.Name + " " + a.Surname, Value = a.ActorId.ToString()});
            }
            ViewBag.ActorId = actorsCombo;

            ViewBag.GenreId = new SelectList(genresRepository.Get(), "GenreId", "Name", film.GenreId);
            return View(film);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FilmId,GenreId,Name,Description,Year")] Film film)
        {
            if (ModelState.IsValid)
            {
                filmsRepository.Update(film);
                filmsRepository.Save();
                return RedirectToAction("Index");
            }
            ViewBag.GenreId = new SelectList(genresRepository.Get(), "GenreId", "Name", film.GenreId);
            return View(film);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Film film = filmsRepository.SingleOrDefault(x => x.FilmId == id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View(film);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Film film = filmsRepository.SingleOrDefault(x => x.FilmId == id);
            filmsRepository.Delete(film);
            filmsRepository.Save();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteRole(int filmId, int actorId)
        {
            ActorRole role = rolesRepository.SingleOrDefault(x => x.FilmId == filmId && x.ActorId == actorId);
            rolesRepository.Delete(role);
            rolesRepository.Save();
            var roles = rolesRepository.Get(x => x.FilmId == filmId);
            return PartialView("_RolesGrid", roles);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult AddRole(ActorRole role)
        {
            var roleToUpdate = rolesRepository.SingleOrDefault(x => x.FilmId == role.FilmId && x.ActorId == role.ActorId);
            if(role.ActorId == 0 || String.IsNullOrEmpty(role.Name))
            {
                var oldRoles = rolesRepository.Get(x => x.FilmId == role.FilmId, includeProperties: "Actor");
                return PartialView("_RolesGrid", oldRoles);
            }
            if (roleToUpdate == null)
            {
                rolesRepository.Insert(role);
                
            }
            else
            {
                roleToUpdate.Name = role.Name;
                rolesRepository.Update(roleToUpdate);
            }
            rolesRepository.Save();

            var roles = rolesRepository.Get(x => x.FilmId == role.FilmId, includeProperties:"Actor");
            return PartialView("_RolesGrid", roles);
        }

        [Authorize]
        public ActionResult AddRating(Rating rating)
        {
            rating.UserId = User.Identity.GetUserId();
            var ratingToUpdate = ratingsRepository.SingleOrDefault(x => x.FilmId == rating.FilmId && x.UserId == rating.UserId);

            if (ratingToUpdate == null)
            {
                ratingsRepository.AddRating(rating);
            }
            else
            {
                ratingToUpdate.Comment = rating.Comment;
                ratingToUpdate.UserRating = rating.UserRating;
                ratingsRepository.UpdateRating(ratingToUpdate);
            }
            ratingsRepository.Save();

            var ratings = ratingsRepository.Get(x => x.FilmId == rating.FilmId, includeProperties:"User");
            return PartialView("_RatingsList", ratings);
        }
    }
}
