﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FilmNetWeb.DAL;
using FilmNetWeb.Models.Entities;
using FilmNetWeb.DAL.Repositories;
using FilmNetWeb.Tools;
using PagedList;
using FilmNetWeb.DAL.Abstract;

namespace FilmNetWeb.Controllers
{
    public class ActorsController : Controller
    {
        private IRepository<Actor> repository;

        public ActorsController(IRepository<Actor> _repository)
        {
            this.repository = _repository;
        }

        public ActionResult Index(string sortOrder, string searchString, int? page)
        {
            var actorsList = GetActors(sortOrder, searchString, page);

            return View("Index", actorsList);
        }
        
        public ActionResult ActorsList(string sortOrder, string searchString, int? page)
        {
            var actorsList = GetActors(sortOrder, searchString, page);
            return PartialView("_ActorsList", actorsList);
        }

        [NonAction]
        private IPagedList<Actor> GetActors(string sortOrder, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentSearchString = searchString;

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.SurnameSortParm = sortOrder == "surname" ? "surname_desc" : "surname";

            var actorsFilter = ExpressionBuilder.BuildActorFilter(searchString);
            var sortActors = ExpressionBuilder.BuildActorOrderBy(sortOrder);

            var actorsList = repository.Get(actorsFilter, sortActors);

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return actorsList.ToPagedList(pageNumber, pageSize);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = repository.SingleOrDefault(x => x.ActorId == id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Surname,Description")] Actor actor)
        {
            if (ModelState.IsValid)
            {
                repository.Insert(actor);
                repository.Save();
                return RedirectToAction("Index");
            }

            return View(actor);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = repository.SingleOrDefault(x => x.ActorId == id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ActorId,Name,Surname,Description")] Actor actor)
        {
            if (ModelState.IsValid)
            {
                repository.Update(actor);
                repository.Save();
                return RedirectToAction("Index");
            }
            return View(actor);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = repository.SingleOrDefault(x => x.ActorId == id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Actor actor = repository.SingleOrDefault(x => x.ActorId == id);
            repository.Delete(actor);
            repository.Save();
            return RedirectToAction("Index");
        }
    }
}
