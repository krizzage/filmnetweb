﻿using FilmNetWeb.DAL;
using FilmNetWeb.DAL.Abstract;
using FilmNetWeb.DAL.Repositories;
using FilmNetWeb.Models.Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmNetWeb.Controllers
{
    public class SystemController : Controller
    {
        private IRepository<Log> logsRepository;

        public SystemController(IRepository<Log> _logsRepository)
        {
            this.logsRepository = _logsRepository;
        }

        public ActionResult Index(int ? page)
        {
            var logs = logsRepository.Get(orderBy: list => list.OrderByDescending(x => x.Date));

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            var logsList = logs.ToPagedList(pageNumber, pageSize);

            return View(logsList);
        }
    }
}