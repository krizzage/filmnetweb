﻿using FilmNetWeb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace FilmNetWeb.Tools
{
    public static class ExpressionBuilder
    {
        public static Expression<Func<Film, bool>> BuildFilmFilter(string _name, string _genre)
        {
            if (String.IsNullOrEmpty(_name) == false && String.IsNullOrEmpty(_genre) == false)
            {
                return x => x.Name.Contains(_name) && x.Genre.Name.Contains(_genre);
            }
            else if (String.IsNullOrEmpty(_name) == false)
            {
                return x => x.Name.Contains(_name);
            }
            else if (String.IsNullOrEmpty(_genre) == false)
            {
                return x => x.Genre.Name.Contains(_genre);
            }
            return null;
        }

        public static Func<IQueryable<Film>, IOrderedQueryable<Film>> BuildFilmOrderBy(string orderby)
        {
            switch (orderby)
            {
                case "name_desc":
                    return films => films.OrderByDescending(f => f.Name);
                case "year":
                    return films => films.OrderBy(f => f.Year);
                case "year_desc":
                    return films => films.OrderByDescending(f => f.Year);
                case "average":
                    return films => films.OrderBy(f => f.Average);
                case "average_desc":
                    return films => films.OrderByDescending(f => f.Average);
                default:
                    return films => films.OrderBy(f => f.Name);
            }
        }

        public static Expression<Func<Actor, bool>> BuildActorFilter(string search)
        {
            if (String.IsNullOrEmpty(search) == false)
                return x => (x.Name + " " + x.Surname).Contains(search);

            return null;
        }

        public static Func<IQueryable<Actor>, IOrderedQueryable<Actor>> BuildActorOrderBy(string orderby)
        {
            switch (orderby)
            {
                case "name_desc":
                    return actors => actors.OrderByDescending(x => x.Name);
                case "surname_desc":
                    return actors => actors.OrderByDescending(x => x.Surname);
                case "surname":
                    return actors => actors.OrderBy(x => x.Surname);
                default:
                    return actors => actors.OrderBy(f => f.Name);
            }
        }
    }
}
