﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmNetWeb.Tools
{
    public enum InvitationValidationMessage
    {
        Success,
        UserDoNotExists,
        UserIsAlreadyFriend,
        UserIsAlreadyInvited,
        UserHasAlreadyInvitedYou,
        CannotInviteYourself,
        Error
    }

}