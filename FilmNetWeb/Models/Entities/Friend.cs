﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FilmNetWeb.Models.Entities
{
    public class Friend
    {
        public Friend()
        {

        }

        [Key]
        [Column(Order = 0)]
        public string SenderId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string ReceiverId { get; set; }

        public bool? IsAccepted { get; set; }
    }
}