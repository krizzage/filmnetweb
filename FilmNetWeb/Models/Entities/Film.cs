﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNetWeb.Models.Entities
{
    public class Film
    {
        public Film()
        {
            Roles = new List<ActorRole>();
            Ratings = new List<Rating>();
        }

        public int FilmId { get; set; }
        [Required(ErrorMessage ="Gatunek filmu jest wymagany")]
        public int GenreId { get; set; }
        [Required(ErrorMessage ="Tytuł filmu jest wymagany")]
        [Display(Name = "Tytuł")]
        public string Name { get; set; }
        [Display(Name = "Średnia")]
        public double Average { get; set; }
        [Display(Name = "Opis")]
        public string Description { get; set; }
        [Required(ErrorMessage ="Rok wydania jest wymagany")]
        [Display(Name = "Rok wydania")]
        public int Year { get; set; }
        
        public virtual Genre Genre { get; set; }
        public virtual ICollection<ActorRole> Roles { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
