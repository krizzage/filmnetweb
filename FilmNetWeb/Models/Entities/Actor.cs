﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNetWeb.Models.Entities
{
    public class Actor
    {
        public Actor()
        {
            Roles = new List<ActorRole>();
        }

        public int ActorId { get; set; }
        [Required(ErrorMessage ="Imię jest wymagane")]
        [Display(Name = "Imię")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Nazwisko jest wymagane")]
        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }
        [Display(Name = "Opis")]
        public string Description { get; set; }

        public virtual ICollection<ActorRole> Roles { get; set; }
    }
}
