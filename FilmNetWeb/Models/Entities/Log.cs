﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNetWeb.Models.Entities
{
    public class Log
    {
        public int LogId { get; set; }
        [Display(Name = "Data wydarzenia")]
        public DateTime Date { get; set; }
        [Display(Name = "Akcja")]
        public string Action { get; set; }
    }
}
