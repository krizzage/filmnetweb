﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNetWeb.Models.Entities
{
    public class Genre
    {
        public Genre()
        {
            Films = new List<Film>();
        }

        public int GenreId { get; set; }
        [Display(Name ="Gatunek")]
        public string Name { get; set; }

        public virtual ICollection<Film> Films { get; set; }
    }
}
