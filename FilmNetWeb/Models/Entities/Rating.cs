﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmNetWeb.Models.Entities
{
    public class Rating
    {
        [Key]
        [Column(Order = 0)]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int FilmId { get; set; }

        [Range(1, 5)]
        [Display(Name = "Ocena")]
        public int UserRating { get; set; }
        [Display(Name = "Komentarz")]
        public string Comment { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual Film Film { get; set; }
    }
}
