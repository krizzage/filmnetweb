﻿using FilmNetWeb.DAL;
using FilmNetWeb.DAL.Repositories;
using FilmNetWeb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmNetWeb.Models.ViewModels
{
    public class PendingInvitationViewModel
    {
        public string SenderId { get; set; }
        public string SenderName { get; set; }


        public PendingInvitationViewModel(Friend pendingInvitation )
        {
            Initialize(pendingInvitation);
        }

        private void Initialize(Friend pendingInvitation)
        {
            using (var userRepository = new UserRepository(new FilmNetContext()))
            {
                var sender = userRepository.GetByID(pendingInvitation.SenderId);
                SenderId = sender.Id;
                SenderName = sender.UserName;
            }
        }
    }
}