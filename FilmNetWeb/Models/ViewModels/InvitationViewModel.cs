﻿using FilmNetWeb.DAL;
using FilmNetWeb.DAL.Repositories;
using FilmNetWeb.Models.Entities;
using FilmNetWeb.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FilmNetWeb.Models.ViewModels
{
    public class InvitationViewModel
    {
        [Required(ErrorMessage ="Wprowadź nazwę użytkownika")]
        [Display(Name = "Login")]
        public string UserName { get; set; }

        public InvitationValidationMessage Validate(string senderLogin)
        {
            using (UserRepository repository= new UserRepository(new FilmNetContext()))
            {
                var receiver = repository.SingleOrDefault(x => x.UserName == UserName);
                IEnumerable<Friend> receiverInvitations = new List<Friend>();

                var sender = repository.SingleOrDefault(x => x.UserName == senderLogin);
                var senderInvitations = repository.GetPendingInvitations(sender.Id);
                var senderFriends = repository.GetFriends(sender.Id);

                if (receiver != null)
                {
                    receiverInvitations = repository.GetPendingInvitations(receiver.Id);
                }
                if (receiver == null)
                {
                    return InvitationValidationMessage.UserDoNotExists;
                }
                else if (senderFriends.SingleOrDefault(x => x.UserName == UserName) != null)
                {
                    return InvitationValidationMessage.UserIsAlreadyFriend;
                }
                else if (receiverInvitations.SingleOrDefault(x => x.SenderId == sender.Id) != null)
                {
                    return InvitationValidationMessage.UserIsAlreadyInvited;
                }
                else if (senderInvitations.SingleOrDefault(x => x.SenderId == receiver.Id) != null)
                {
                    return InvitationValidationMessage.UserHasAlreadyInvitedYou;
                }
                else if (receiver.UserName == senderLogin)
                {
                    return InvitationValidationMessage.CannotInviteYourself;
                }

                return InvitationValidationMessage.Success;
            }
        }
    }
}