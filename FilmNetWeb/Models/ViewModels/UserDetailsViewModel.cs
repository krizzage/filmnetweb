﻿using FilmNetWeb.DAL;
using FilmNetWeb.DAL.Repositories;
using FilmNetWeb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmNetWeb.Models.ViewModels
{
    public class UserDetailsViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }

        public List<Rating> ratings;

        public UserDetailsViewModel(ApplicationUser user)
        {
            Initialize(user);
        }

        private void Initialize(ApplicationUser user)
        {
            UserId = user.Id;
            UserName = user.UserName;
            ratings = user.Ratings.ToList();
        }
    }
}