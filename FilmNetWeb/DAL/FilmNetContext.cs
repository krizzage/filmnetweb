﻿using System.Data.Entity;
using System.Linq;
using System;
using FilmNetWeb.Models;
using FilmNetWeb.Models.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FilmNetWeb.DAL
{
    public class FilmNetContext : IdentityDbContext<ApplicationUser>
    {
        public FilmNetContext() :
            base("FilmNetConnectionString", throwIfV1Schema: false)
        {
            Database.SetInitializer<FilmNetContext>(new FilmNetDbInitializer());
        }

        public DbSet<Actor> Actors { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<ActorRole> ActorRoles { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Friend> Friends { get; set; }

        public static FilmNetContext Create()
        {
            return new FilmNetContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Senders)
                .WithRequired()
                .HasForeignKey(e => e.ReceiverId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Receivers)
                .WithRequired()
                .HasForeignKey(e => e.SenderId)
                .WillCascadeOnDelete(false);
        }

        public override int SaveChanges()
        {
            var addedAuditedEntities = ChangeTracker.Entries()
                    .Where(p => p.State == EntityState.Added)
                    .Select(p => p.Entity);
            var modifiedAuditedEntities = ChangeTracker.Entries()
                  .Where(p => p.State == EntityState.Modified)
                  .Select(p => p.Entity);
            var deletedAuditedEntities = ChangeTracker.Entries()
                    .Where(p => p.State == EntityState.Deleted)
                    .Select(p => p.Entity);
            var now = DateTime.Now;


            foreach (var added in addedAuditedEntities)
            {
                if (added is Film)
                {
                    var i = (Film)added;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Dodanie filmu : " + i.Name });
                }
                if (added is Actor)
                {
                    var i = (Actor)added;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Dodanie aktora : " + i.Name + i.Surname });
                }
            }

            foreach (var modified in modifiedAuditedEntities)
            {
                if (modified is Film)
                {
                    var i = (Film)modified;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Modyfikacja filmu id : " + i.FilmId });
                }
                if (modified is Actor)
                {
                    var i = (Actor)modified;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Modyfikacja aktora id : " + i.ActorId + i.Surname });
                }
            }

            foreach (var deleted in deletedAuditedEntities)
            {
                if (deleted is Film)
                {
                    var i = (Film)deleted;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Usuniecie filmu : " + i.Name });
                }
                if (deleted is Actor)
                {
                    var i = (Actor)deleted;
                    Logs.Add(new Log() { Date = DateTime.Now, Action = "Usuniecie aktora : " + i.Name + i.Surname });
                }
            }

            return base.SaveChanges();

        }

    }
}
