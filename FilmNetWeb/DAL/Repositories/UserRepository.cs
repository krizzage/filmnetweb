﻿using FilmNetWeb.DAL.Abstract;
using FilmNetWeb.Models;
using FilmNetWeb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FilmNetWeb.DAL.Repositories
{
    public class UserRepository : Repository<ApplicationUser>, IUserRepository
    {
        public UserRepository(FilmNetContext context) : base(context)
        {
        }

        public void AddInvitation(string senderId, string receiverId)
        {
            context.Friends.Add(new Friend() { ReceiverId = receiverId, SenderId = senderId, IsAccepted = null });
        }
        
        public IEnumerable<ApplicationUser> GetFriends(string userId)
        {
            var relation = context.Friends.Where(x => (x.SenderId == userId || x.ReceiverId == userId) && x.IsAccepted == true);
            return context.Users.Join(relation, x => x.Id, y => y.SenderId == userId ? y.ReceiverId : y.SenderId, (x, y) => x).ToList();
        }

        public IEnumerable<Friend> GetPendingInvitations(string userId)
        {
            IQueryable<Friend> query = context.Friends;
            query = query.Where(x => x.ReceiverId == userId && x.IsAccepted == null);

            return query.ToList();
        }

        public void AcceptInvitation(Friend invitation)
        {
            var inv = context.Friends.Single(x => x.SenderId == invitation.SenderId && x.ReceiverId == invitation.ReceiverId);
            inv.IsAccepted = true;
        }

        public Friend GetInvitationByUsersId(string sender, string receiver)
        {
            return context.Friends.SingleOrDefault(x => x.SenderId == sender && x.ReceiverId == receiver);
        }

        public void DeclineInvitation(Friend invitation)
        {
            var inv = context.Friends.Single(x => x.SenderId == invitation.SenderId && x.ReceiverId == invitation.ReceiverId);
            context.Friends.Remove(inv);
        }
    }
}
