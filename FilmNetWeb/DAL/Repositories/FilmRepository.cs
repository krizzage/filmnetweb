﻿using FilmNetWeb.DAL.Abstract;
using FilmNetWeb.Models.Entities;
using RefactorThis.GraphDiff;

namespace FilmNetWeb.DAL.Repositories
{
    public class FilmRepository : Repository<Film>, IFilmRepository
    {
        public FilmRepository(FilmNetContext context) : base(context)
        {

        }

        public void UpdateFilm(Film film)
        {
            context.UpdateGraph(film, map => map.OwnedCollection(x => x.Roles));
        }
    }
}
