﻿using FilmNetWeb.DAL.Abstract;
using FilmNetWeb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmNetWeb.DAL.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(FilmNetContext context) : base(context)
        {

        }

        public void AddRating(Rating rating)
        {
            this.Insert(rating);
            this.Save();

            UpdateFilmAverage(rating.FilmId);
        }

        public void UpdateRating(Rating rating)
        {
            this.Update(rating);
            this.Save();

            UpdateFilmAverage(rating.FilmId);
        }

        private void UpdateFilmAverage(int filmId)
        {
            using (var filmRepository = new Repository<Film>(new FilmNetContext()))
            {
                var film = filmRepository.GetByID(filmId);
                var filmRatings = film.Ratings.ToList();

                film.Average = (double) filmRatings.Sum(x => x.UserRating) / (double) filmRatings.Count;

                filmRepository.Update(film);
                filmRepository.Save();
            }
        }
    }
}