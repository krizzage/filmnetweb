﻿using FilmNetWeb.Models;
using FilmNetWeb.Models.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FilmNetWeb.DAL
{
    public class FilmNetDbInitializer : CreateDatabaseIfNotExists<FilmNetContext>
    {
        protected override void Seed(FilmNetContext context)
        {
            
        }
    }
}
