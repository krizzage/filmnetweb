﻿using FilmNetWeb.Models.Entities;

namespace FilmNetWeb.DAL.Abstract
{
    public interface IFilmRepository : IRepository<Film>
    {
        void UpdateFilm(Film film);
    }
}
