﻿using FilmNetWeb.Models;
using FilmNetWeb.Models.Entities;
using System.Collections.Generic;

namespace FilmNetWeb.DAL.Abstract
{ 
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        void AddInvitation(string senderId, string receiverId);
        IEnumerable<ApplicationUser> GetFriends(string userId);
        IEnumerable<Friend> GetPendingInvitations(string userId);
        void AcceptInvitation(Friend invitation);
        Friend GetInvitationByUsersId(string sender, string receiver);
        void DeclineInvitation(Friend invitation);
    }
}
