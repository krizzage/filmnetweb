﻿using FilmNetWeb.Models.Entities;

namespace FilmNetWeb.DAL.Abstract
{
    public interface IRatingRepository : IRepository<Rating>
    {
        void AddRating(Rating rating);
        void UpdateRating(Rating rating);
    }
}
